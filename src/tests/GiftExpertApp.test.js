import React from 'react'
import '@testing-library/jest-dom'
import {shallow} from "enzyme";
import GiftExpertApp from "../GiftExpertApp";

describe('Test del componente GiftExpertApp', function () {

    test('Haciendo el snapshot del comoponente', () => {
        const wrapper = shallow(<GiftExpertApp />);
        expect(wrapper).toMatchSnapshot();
    })

    test('Haciendo el snapshot del comoponente', () => {

        const categories = ['One Punch', 'One Piece'];
        const wrapper = shallow(<GiftExpertApp defaultCategories={categories} />);

        expect(wrapper).toMatchSnapshot();
    })

});