import React from 'react';
import '@testing-library/jest-dom'; /* Se hace solo para que te salgan los autocompletados, SOLO para ello */
import {shallow} from 'enzyme';

import CategoryItem from "../../components/CategoryItem";


describe('Pruebas desde component CategoryItem ', () => {

    const title = "Este es un titulo";
    const url = 'http://web';
    const wrapper = shallow(<CategoryItem title={title} url={url}/>);

    test('Debe mostrar el componente correctamente', () => {

        const wrapper = shallow(<CategoryItem title={title} url={url}/>);
        expect(wrapper).toMatchSnapshot();

    });

    test('El titulo es el correcto', () => {

        const wrapperText = wrapper.find('p').text();
        expect(wrapperText).toBe(title);

    });

    //Para hacer un test sobre los atributos dentro de un tag html, se le considerar como prop, (atributo o propiedad)
    test('Las propiedades o atributos de la etiqueta img, se asignaron correctamente', () => {

        const imgComponent = wrapper.find('img');

        expect(imgComponent.prop('src')).toBe(url);
        expect(imgComponent.prop('alt')).toBe(title);

    });

    // para hacer un test sobre si se asigno correctamente la clase sobre una etiqueta html (un class)
    test( 'La clase animate__fadeIn se ingreso correctamente al div de CategoryItem', ()=>{

        const divComponent = wrapper.find('div');
        expect(divComponent.prop('className').includes('animate__fadeIn')).toBe(true);

    })

});

