import '@testing-library/jest-dom';
import {shallow} from "enzyme";

import AddCategory from "../../components/AddCategory";

describe('test in AddCategory', () => {

    const setCategories = jest.fn(); // Equals to  ()=>{}
    let wrapper;

    beforeEach(() => {
        jest.clearAllMocks();
        wrapper = shallow(<AddCategory setCategory={setCategories}/>);
    })

    test('debe de mostrarse correctamente', () => {

        expect(wrapper).toMatchSnapshot();

    });

    test('debe cambiar la caja de texto', () => {

        const inputDiv = wrapper.find('input');
        const valorInput = 'Kart';

        inputDiv.simulate('change', {target: {value: valorInput}});
        // console.log(wrapper.find('p').text());
        expect(wrapper.find('p').text().trim()).toBe(valorInput);

    });

    test('No debe de postear con submit', () => {
        wrapper.find('form').simulate('submit', {
            preventDefault() {
            }
        });
        expect(setCategories).not.toHaveBeenCalled();
    });

    test('Debe llamar el setCategories y limpiar la caja de texto', () => {

        const inputValue = 'Bleach';

        // Simulando el llenado del input
        wrapper.find('input').simulate('change', {target: {value: inputValue}})

        // Simulando en submit
        wrapper.find('form').simulate('submit', {
            preventDefault() {
            }
        });

        //que se haya llamado una vez al setCategories
        expect(setCategories).toHaveBeenCalled(); // 1 a mas
        expect(setCategories).toHaveBeenCalledTimes(1); // que solo se haya llamado 1 sola vez
        expect(setCategories).toHaveBeenCalledWith(expect.any(Function)); // cuando espero que se le envie una funcion

        // se limpia el input
        expect(wrapper.find('input').prop('value')).toBe('');

    });

});