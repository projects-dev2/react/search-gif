import React from 'react'
import '@testing-library/jest-dom'
import {shallow} from "enzyme";
import ShowCategory from "../../components/ShowCategory";
import {useFetchGifs} from "../../hooks/useFetchGifs";

jest.mock('../../hooks/useFetchGifs'); // Para que se comporte como mi hook propio


describe('Pruebas en el component ShowCategory', function () {

    const category = "Tokyo Ghoul"
    // const wrapper = shallow(<ShowCategory category={category}/>);

    test('Creando el Snapshot del componente ShowCategor, sin data', () => {

        useFetchGifs.mockReturnValue({
            data: [],
            loading: true
        });

        const wrapper = shallow(<ShowCategory category={category}/>);
        expect(wrapper).toMatchSnapshot();
    })

    test('Debe de mostrar items cuando se carga las imagenes de UseFetchGifs', () => {

        const gifts = [{
            id: '666',
            title: 'GAAA image',
            url: 'GAAA url'
        }, {
            id: '777',
            title: 'RAAA image',
            url: 'RAAA url'
        }]

        useFetchGifs.mockReturnValue({
            data: gifts,
            loading: false
        });

        const wrapper = shallow(<ShowCategory category={category}/>);
        // otra forma pora validar si se cargo la data correctamente, es por medio de si se renderiza el 'p' de Loading
        expect(wrapper).toMatchSnapshot();

        // Veridicar si existe el tag p, osea si se ha creado
        expect(wrapper.find('p').exists()).toBe(false);
        // ver cuantas veces se ha renderizado nuestro propio componente
        expect(wrapper.find('CategoryItem').length).toBe(gifts.length);
    });

});