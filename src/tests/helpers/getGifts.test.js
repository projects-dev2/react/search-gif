import {getGifts} from "../../helpers/getGifts";


describe('Pruebas en getGifts', () => {

    test('debe traer 10 elements', async () => {
         const gift = await getGifts('One Piece')

        expect(gift.length).toBe(5);
    })

    test('Debe devolver un arreglo vacio', async () => {
         const gift = await getGifts('')

        expect(gift.length).toBe(0);
    })
})