import React, {useState} from 'react';
import PropTypes from 'prop-types';

const AddCategory = ({setCategory}) => {

    const [inputValue, setInputValue] = useState('');

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log('En el handleSubmit', e, inputValue);

        if (inputValue.trim().length >= 1) {
            console.log('entre')
            setCategory((list) => [inputValue, ...list]);
            setInputValue('');
        }
    }

    const handleOnChange = (e) => {
        setInputValue(e.target.value);
    }

    return (
        <form onSubmit={handleSubmit}>
            {/*<p>{inputValue}</p>*/}
            <button className="buttonAdd" onClick={handleSubmit}>Add</button>
            <input className="input_search"
                   type="text"
                   value={inputValue}
                   onChange={handleOnChange}
                   placeholder="Element a to search"
            />
        </form>
    )
}

AddCategory.propTypes = {
    setCategory: PropTypes.func.isRequired
}

export default AddCategory;