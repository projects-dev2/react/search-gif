import React from 'react';
import PropTypes from "prop-types";

import {useFetchGifs} from '../hooks/useFetchGifs';
import CategoryItem from './CategoryItem';

const ShowCategory = ({category}) => {

    // Without Destructure
    // const state = useFetchGifs();

    // With Destructure
    const {data, loading} = useFetchGifs(category);

    return (
        <>
            <h3 className="animate__animated animate__fadeIn">{category}</h3>

            {loading && <p className="card animate__animated animate__flash">Loading</p>}

            <div className="card-grid">
                {
                    data.map((img) => (
                        <CategoryItem key={img.id} {...img}/>
                    ))
                }
            </div>
        </>
    )
}

ShowCategory.propTypes = {
    category: PropTypes.string.isRequired,
}

export default ShowCategory;