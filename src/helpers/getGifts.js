export const getGifts = async (category) => {

    const url = `https://api.giphy.com/v1/gifs/search?api_key=tkHpod5kmmjcVD4UHQUJimuz289Mzqdz&q=${encodeURI(category)}&limit=5`;

    const res = await fetch(url);
    const {data} = await res.json();

    return data.map(image => {
        return {
            id: image.id,
            title: image.title,
            url: image.images?.original.url
        }
    });
}