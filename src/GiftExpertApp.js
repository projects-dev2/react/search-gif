import React, {useState} from 'react'
import AddCategory from './components/AddCategory';
import ShowCategory from './components/ShowCategory';

const GiftExpertApp = ({defaultCategories = ["dragon ball"]} ) => {

    const [categories, setCategories] = useState(defaultCategories);

    return (
        <>
            <h2>Search Gift</h2>
            <AddCategory setCategory={setCategories}/>
            <hr/>

            {
                categories.map((category) => (
                    <ShowCategory
                        key={category}
                        category={category}
                    />
                ))
            }
        </>
    )
}

export default GiftExpertApp;
